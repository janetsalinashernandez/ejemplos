<?php
  /* Clase para el acceso a la BD */
  class BD {
    private static $singleton;
    private $conexion;

    public function __construct() {
      $this->conexion = pg_connect("host=".SERVIDORBD." dbname=".BASEDEDATOS." user=".USUARIOBD." password=".PASSWORDBD)
        or die('Error en conexión al SGBD');
    }

    /* La presente clase implementa el Patrón Singleton, el cual
     * mantiene una sola instancia del mismo por lo que al instanciarlo se
     * obtiene el mismo objeto
     */
    public static function singleton() {
      if(!self::$singleton) {
        self::$singleton = new BD();
      }
      return self::$singleton;     
    }

    public function ejecutar($query = "") {
      if (empty($query)) {
        return array();
      }

      $resultado = pg_query($this->conexion, $query);
      if (!$resultado) {
        return array();
      }

      /* Se extrae cada uno de los arreglos (filas) obtenidos desde la BD */
      $tmp = array();
      while ($tupla = pg_fetch_array($resultado, null, PGSQL_ASSOC)) {
        $tmp[] = $tupla;
      }

      return $tmp;
    }
  }
