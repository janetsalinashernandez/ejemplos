<?php
  class Libro {
    private $isbn;
    private $titulo_libro;
    private $ejemplares;
    private $autores;
    public $error;

    private $bd;

    /* En PHP 5.4 (y versiones anteriores) no puedes
     * sobrecargar al método __construct() de una clase
     *
     * PHP genera el mensaje:
     *    PHP Fatal error:  Cannot redeclare Libro::__construct() in
     *    /srv/www/htdocs/.../frame.works.com/ejemplos/clases/02.php on
     *    line x
     *
    public function __construct() {
    }
     */

    public function __construct($isbn = "") {
      $this->isbn = $isbn;
      $this->titulo_libro = "";
      $this->ejemplares = array();
      $this->autores = array();
      $this->error = "";

      /* Cuando se crea un objeto de tipo Libro automaticamente dicho
       * objeto puede contar con acceso a la BD o bien ... **
       */
      //$this->bd = BD::singleton();
    }

    public function getIsbn() {
      return $this->isbn;
    }

    public function getTituloLibro() {
      return $this->titulo_libro;
    }

    public function getEjemplares() {
      return $this->ejemplares;
    }

    public function getAutores() {
      return $this->autores;
    }

    public function cargarDatos() {
      if ($this->isbnValido()) {

        /* ** ... se puede tener acceso a la BD solo cuando se requiere */
        $this->bd = BD::singleton();

        $query = "select isbn, titulo_libro
          from biblioteca.libro
          where isbn = '".$this->isbn."';";
 
        $resultado = $this->bd->ejecutar($query);
        if (empty($resultado)) {
          $this->error = 'Error: el ISBN no se encuentra registrado.';
          return false;
        }

        /* Asignar valores a los atributos del libro con la información de la BD */
        $libro = array_shift($resultado);
        $this->titulo_libro = $libro['titulo_libro'];

        $query = "select clave_ejemplar, conservacion_ejemplar
          from biblioteca.ejemplar
          where isbn = '".$this->isbn."';";

        $this->ejemplares = $this->bd->ejecutar($query);

        $query = "select nombre_autor
          from biblioteca.libro_autor as LA
          inner join biblioteca.autor as A
          on (LA.id_autor = A.id_autor and LA.isbn = '".$this->isbn."');";

        $this->autores = $this->bd->ejecutar($query);

        return true;
      }
    }

    private function isbnValido() {
      if (empty($this->isbn)) {
        $this->error = 'Error: no se ha indicado el ISBN.';
        return false;
      } else if (mb_strlen($this->isbn) != 10) {
        $this->error = 'Error: el ISBN debe estar conformado por 10 caracteres';
        return false;
      } else if (!is_numeric($this->isbn)) {
        $this->error = 'Error: el ISBN solo debe estar conformado por 10 números.';
        return false;
      }
      return true;
    }

  }
