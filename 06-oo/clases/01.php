<?php
  /* Múltiples clases pueden ser definidas en un mismo script de PHP */
  class Prueba1 {
    public $atributo1;
    public $atributo2;
    public $atributo3;

    public function metodo1() {
      return "";
    }

    public function metodo2() {
      return "";
    }

    public function metodo3() {
      return "";
    }

  }

  class Prueba2 {
    private $atributo1 = "";
    private $atributo2 = "";
    public $atributo3 = "";

    public function __construct($valor1, $valor2, $valor3) {
      $this->atributo1 = $valor1;
      $this->atributo2 = $valor2;
      $this->atributo3 = $valor3;
    }

    public function get_atributo1() {
      return $this->atributo1;
    }

    public function get_atributo2() {
      return $this->atributo2;
    }

    public function get_atributo3() {
      return $this->atributo3;
    }

    public function set_atributo1($valor1) {
      $this->atributo1 = $valor1;
    }

    public function otro_metodo() {
      echo $this->atributo1 . " " . $this->atributo2 . " " . $this->atributo3;
    }
  }

/* No se requiere cerrar la etiqueta de PHP (?>) y de hecho
* una recomendación es NO usarla.
 */
