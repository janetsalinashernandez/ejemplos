<!DOCTYPE html>
<html>
  <head>
    <meta charset=utf-8>
  </head>
  <body>
  <a href="http://www.php.net/manual/es/language.variables.superglobals.php">
    http://www.php.net/manual/es/language.variables.superglobals.php
  </a>
  <hr />

  <strong>$_SERVER</strong>
  <a href="http://www.php.net/manual/es/reserved.variables.server.php">
    http://www.php.net/manual/es/reserved.variables.server.php
  </a>
<?php

  echo "<pre>";
  var_dump($_SERVER);
  echo "</pre>";

  echo "<pre>";
  foreach ($_SERVER as $llave => $valor) {
    echo "Llave= ".$llave."\t\t\tValor= ".$valor."\n";
  }
  echo "</pre>";

?>

  </body>
</html>
